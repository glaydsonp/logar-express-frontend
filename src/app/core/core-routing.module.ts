import { AnunciosModule } from 'src/app/pages/anuncios/anuncios.module';
import { FretesModule } from 'src/app/pages/fretes/fretes.module';
import { AuthGuard } from 'src/app/services/guards/auth.guard';
import { CadastroModule } from 'src/app/pages/cadastro/cadastro.module';
import { LoginModule } from 'src/app/pages/login/login.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from 'src/app/services/guards/login.guard';
import { PublicidadesModule } from '../pages/publicidades/publicidades.module';
import { AnuncianteGuard } from '../services/guards/anunciante.guard';
import { RecuperarSenhaModule } from '../pages/recuperar-senha/recuperar-senha.module';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => LoginModule,
    canActivate: [LoginGuard]
  },
  {
    path: 'cadastro',
    loadChildren: () => CadastroModule,
    canActivate: [LoginGuard]
  },
  {
    path: 'fretes',
    loadChildren: () => FretesModule,
    canActivate: [AuthGuard]
  },
  {
    path: 'publicidades',
    loadChildren: () => PublicidadesModule,
    canActivate: [AuthGuard]
  },
  {
    path: 'recuperar-senha',
    loadChildren: () => RecuperarSenhaModule,
  },
  {
    path: 'anuncios',
    loadChildren: () => AnunciosModule,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
