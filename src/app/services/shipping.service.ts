import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IShipping } from 'src/app/models/shipping';

@Injectable({
  providedIn: 'root'
})
export class ShippingService {
  api = 'frete';
  baseUrl = `${environment.baseUrl}/${this.api}`;

  approvalBody = {
    frete_aprovado: true
  };

  constructor(private http: HttpClient) {}

  save(form: IShipping) {
    return this.http.post<any>(`${this.baseUrl}`, form);
  }

  getAll() {
    return this.http.get<{ fretes: IShipping[] }>(`${this.baseUrl}`);
  }

  approve(id: number) {
    return this.http.put<any>(`${this.baseUrl}/${id}`, this.approvalBody);
  }

  delete(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
