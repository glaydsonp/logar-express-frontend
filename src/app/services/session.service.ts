import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ISession } from 'src/app/models/session';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor(private _router: Router) {}

  setSession(session: ISession, keepConnected = false) {
    const storage = keepConnected ? localStorage : sessionStorage;

    storage.setItem(`${environment.prefix}_session`, JSON.stringify(session));
  }

  reset() {
    localStorage.clear();
    sessionStorage.clear();
  }

  logout() {
    this.reset();

    this._router.navigate(['/login']);
  }
}
