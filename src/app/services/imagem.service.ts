import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RespostaUploadImagem } from '../models/imagem.interface';

@Injectable({
  providedIn: 'root'
})
export class ImagemService {
  private baseUrl = `${environment.baseUrl}/imagem`;

  constructor(private httpClient: HttpClient) {}

  uploadImagem(file: File): Observable<RespostaUploadImagem> {
    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append('file', file, file.name);

    // Make http post request over api
    // with formData as req
    return this.httpClient.post<RespostaUploadImagem>(this.baseUrl, formData);
  }
}
