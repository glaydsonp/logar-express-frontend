import { RoleEnum } from 'src/app/models/roles';
import { Router } from '@angular/router';
import { SessionService } from './session.service';
import { SnackbarService } from './snackbar.service';
import { IToken } from 'src/app/models/user';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ILogin } from 'src/app/models/login';
import { HttpClient } from '@angular/common/http';
import jwtDecode from 'jwt-decode';
import { ISession } from 'src/app/models/session';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  api = 'auth';
  baseUrl = `${environment.baseUrl}/${this.api}`;

  constructor(
    private _http: HttpClient,
    private _snackbarService: SnackbarService,
    private _sessionService: SessionService,
    private _router: Router
  ) {}

  login(form: ILogin) {
    this._http.post<IToken>(`${this.baseUrl}/login`, form).subscribe(
      (res) => {
        // const user = this.decoder(res.access_token);
        const user = res.dados;

        const session = {
          access_token: res.access_token,
          expires_in: Date.now() + res.expires_in * 1000,
          dados: user,
          role: res.role
        } as ISession;

        this._sessionService.setSession(session);

        const role = session.role;

        role === RoleEnum.Caminhoneiro
          ? this._router.navigate(['/fretes'])
          : role === RoleEnum.Anunciante
          ? this._router.navigate(['/publicidades'])
          : this._router.navigate(['/anuncios']);
      },
      (err) => {
        let message: string;

        if (err.status === 401) {
          message = 'Login/senha inválidos.';
        } else {
          message = err.message;
        }

        this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        console.log('err', err);
      }
    );
  }

  recuperarSenha(email: string) {
    this._http
      .post<{ mensagem: string }>(`${this.baseUrl}/reset-password`, { email })
      .subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );
          this._router.navigate(['/login']);
        },
        (err) => {
          let message: string;

          if (err.status === 401) {
            message = 'Login/senha inválidos.';
          } else {
            message = err.message;
          }

          this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        }
      );
  }
}
