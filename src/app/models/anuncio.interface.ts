export interface IAnuncio {
  id: number;
  titulo: string;
  segmento: string;
  preco: number;
  descricao: string;
  url: string;
  tempo_dias: number;
  caminho_arquivo: string;
  imagem_vip?: string;
  publicidade_aprovada: number;
  codigo_publicidade: string;
}

export interface IAnuncioForm {
  caminho_arquivo: string;
  descricao: string;
  segmento: string;
  url: string;
  titulo: string;
  tempo_dias: number;
  preco: number;
  empresa_id: number;
}
