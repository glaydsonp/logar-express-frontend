import { Component, Input, OnInit } from '@angular/core';
import { IAnuncio } from 'src/app/models/anuncio.interface';
import { RoleEnum } from 'src/app/models/roles';
import { AnunciosService } from 'src/app/services/anuncios.service';
import { AuthService } from 'src/app/services/auth.service';
import { SessionService } from 'src/app/services/session.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-publicidades-item-listagem',
  templateUrl: './publicidades-item-listagem.component.html',
  styleUrls: ['./publicidades-item-listagem.component.scss']
})
export class PublicidadesItemListagemComponent implements OnInit {
  @Input() publicidade!: IAnuncio;
  isAdmin = false;
  isAnunciante = false;

  constructor(
    private authService: AuthService,
    private anunciosService: AnunciosService
  ) {}

  ngOnInit() {
    this.isAdmin = this.authService.getRole() === RoleEnum.Admin;
    this.isAnunciante = this.authService.getRole() === RoleEnum.Anunciante;
  }

  getImageLink() {
    return `${environment.baseUrl}/imagem/${this.publicidade.caminho_arquivo}`;
  }

  aprovarAnuncio() {
    this.anunciosService.aprovarAnuncio(this.publicidade.id);
  }

  removerAnuncio() {
    this.anunciosService.deletarAnuncio(this.publicidade.id);
  }

  comprarProduto() {
    const a = document.createElement('a');
    a.href = this.publicidade.url;
    a.target = '_blank';
    a.click();
    a.remove();
  }
}
