import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { AnunciosService } from 'src/app/services/anuncios.service';
import { ImagemService } from 'src/app/services/imagem.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { TruckerService } from 'src/app/services/trucker.service';
import { NovoAnuncioDialogComponent } from '../../anuncios/novo-anuncio-dialog/novo-anuncio-dialog.component';
import { NovaPublicidadeDialogComponent } from '../nova-publicidade-dialog/nova-publicidade-dialog.component';

@Component({
  selector: 'app-publicidades-cadastro',
  templateUrl: './publicidades-cadastro.component.html',
  styleUrls: ['./publicidades-cadastro.component.scss']
})
export class PublicidadesCadastroComponent implements OnInit {
  form = new FormGroup({});
  segmentos = [
    'Acessórios',
    'Peças de Linha Pesada',
    'Pneus',
    'Rodas',
    'Som automotivo',
    'Motor',
    'Câmbio',
    'Freios',
    'Suspensão',
    'Cabine',
    'Elétrica',
    'Turbina',
    'Lataria',
    'Cardan',
    'Diferencial',
    'Direção',
    'Conexões',
    'Baterias',
    'Escapamento',
    'Outros'
  ];
  quantidadeDias = [10, 20, 30];
  file?: File;
  imagemUrl = '';
  imagemUrl2 = '';

  constructor(
    private _fb: FormBuilder,
    private _shippingService: ShippingService,
    public truckerService: TruckerService,
    private _snackbarService: SnackbarService,
    private anunciosService: AnunciosService,
    private imagemService: ImagemService,
    public dialog: MatDialog,
    private _router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  createForm() {
    this.form = this._fb.group({
      dadosAnuncio: this._fb.group({
        titulo: [null, Validators.required],
        segmento: [null, Validators.required]
      }),
      produto: this._fb.group({
        preco: [null, Validators.required],
        descricao: [null, Validators.required],
        url: [null, Validators.required]
      }),
      finalizar: this._fb.group({
        tempo_dias: [null, Validators.required],
        arteAnuncio: [null, Validators.required],
        arteAnuncio2: [null, Validators.required]
      })
    });
  }

  onChange(event: any) {
    this.file = event?.target?.files[0];

    if (this.file && this.file.size > 1024000) {
      this._snackbarService.openSnackBar(
        'Imagem muito grande, por favor escolha uma imagem com menos de 1024KB.',
        SnackbarType.Danger
      );
      return;
    }

    if (this.file) {
      this.imagemService.uploadImagem(this.file).subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );
          this.imagemUrl = res.uploadedImageResponse.image_name;
        },
        (err) => {
          this._snackbarService.openSnackBar(
            err.error.mensagem,
            SnackbarType.Danger
          );
        }
      );
    }
  }

  onChangeImagem2(event: any) {
    this.file = event?.target?.files[0];

    if (this.file && this.file.size > 1024000) {
      this._snackbarService.openSnackBar(
        'Imagem muito grande, por favor escolha uma imagem com menos de 1024KB.',
        SnackbarType.Danger
      );
      return;
    }

    if (this.file) {
      this.imagemService.uploadImagem(this.file).subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );
          this.imagemUrl2 = res.uploadedImageResponse.image_name;
        },
        (err) => {
          this._snackbarService.openSnackBar(
            err.error.mensagem,
            SnackbarType.Danger
          );
        }
      );
    }
  }

  criarAnuncio() {
    if (!this.form.valid) {
      this._snackbarService.openSnackBar(
        'Fomulário inválido!',
        SnackbarType.Danger
      );

      return;
    }

    const form = Object.assign(
      this.form.get('dadosAnuncio')?.value,
      this.form.get('produto')?.value,
      this.form.get('finalizar')?.value
    );

    if (form && form.preco) {
      form.preco = +form.preco
        .replace('R$', '')
        .replace('.', '')
        .replace(',', '.');
    }

    this.anunciosService
      .adicionarAnuncio({
        caminho_arquivo: this.imagemUrl,
        imagem_vip: this.imagemUrl2,
        ...form
      })
      .subscribe(
        (res) => {
          this._snackbarService.openSnackBar(
            res.mensagem,
            SnackbarType.Success
          );

          if (res && res.publicidade) {
            this.dialog.open(NovaPublicidadeDialogComponent, {
              data: res.publicidade
            });
          } else {
            this._snackbarService.openSnackBar(
              'Algo deu errado, por favor tente novamente.',
              SnackbarType.Danger
            );

            this._router.navigate(['/publicidades']);
          }
        },
        (error) => {
          const message = error.error.mensagem;

          this._snackbarService.openSnackBar(message, SnackbarType.Danger);
        }
      );
  }
}
