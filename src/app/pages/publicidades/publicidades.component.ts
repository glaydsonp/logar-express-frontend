import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAnuncio } from 'src/app/models/anuncio.interface';
import { RoleEnum } from 'src/app/models/roles';
import { IShipping } from 'src/app/models/shipping';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { AnunciosService } from 'src/app/services/anuncios.service';
import { AuthService } from 'src/app/services/auth.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-publicidades',
  templateUrl: './publicidades.component.html',
  styleUrls: ['./publicidades.component.scss']
})
export class PublicidadesComponent implements OnInit {
  fretes: IShipping[] = [];
  anunciosVip: IAnuncio[] = [];
  anuncios: IAnuncio[] = [];

  constructor(
    private _router: Router,
    public shippingService: ShippingService,
    private anunciosService: AnunciosService,
    private _snackbarService: SnackbarService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getAnuncios();
  }

  getImageLink(publicidade: IAnuncio) {
    return publicidade.imagem_vip
      ? `${environment.baseUrl}/imagem/${publicidade.imagem_vip}`
      : `${environment.baseUrl}/imagem/${publicidade.caminho_arquivo}`;
  }

  goToUrl(anuncio: IAnuncio) {
    const a = document.createElement('a');
    a.href = anuncio.url;
    a.target = '_blank';
    a.click();
  }

  adicionarPublicidade() {
    this._router.navigate(['/publicidades/novo']);
  }

  getAnuncios() {
    this.anunciosService.listarTodosAnuncios().subscribe((res) => {
      this.anuncios = res.publicidades;
    });

    this.anunciosService.listarAnunciosVip().subscribe((res) => {
      this.anunciosVip = res.publicidades;
    });
  }

  approveShipping(id: any) {
    this.shippingService.approve(id).subscribe(
      (res: any) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);
      },
      (err) => {
        this._snackbarService.openSnackBar(
          err.error?.mensagem ?? err.message,
          SnackbarType.Danger
        );
        console.log('err', err);
      }
    );

    // this.getFretes();
  }

  deleteShipping(id: any) {
    this.shippingService.delete(id).subscribe(
      (res: any) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);
      },
      (err) => {
        this._snackbarService.openSnackBar(
          err.error?.mensagem ?? err.message,
          SnackbarType.Danger
        );
        console.log('err', err);
      }
    );

    // this.getFretes();
  }
}
