import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicidadesCadastroComponent } from './publicidades-cadastro/publicidades-cadastro.component';
import { PublicidadesComponent } from './publicidades.component';

const routes: Routes = [
  {
    path: '',
    component: PublicidadesComponent
  },
  {
    path: 'novo',
    component: PublicidadesCadastroComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicidadesRoutingModule {}
