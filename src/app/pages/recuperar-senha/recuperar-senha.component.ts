import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-recuperar-senha',
  templateUrl: './recuperar-senha.component.html',
  styleUrls: ['./recuperar-senha.component.scss']
})
export class RecuperarSenhaComponent implements OnInit {
  form = new FormGroup({});

  constructor(
    private router: Router,
    private _fb: FormBuilder,
    public loginService: LoginService
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  recuperarSenha() {
    this.loginService.recuperarSenha(this.form.get('email')!.value);
  }

  createForm() {
    this.form = this._fb.group({
      email: [null, [Validators.required, Validators.email]]
    });
  }
}
