import { Router } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-novo-anuncio-dialog',
  templateUrl: './novo-anuncio-dialog.component.html',
  styleUrls: ['./novo-anuncio-dialog.component.scss']
})
export class NovoAnuncioDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NovoAnuncioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  sendToHome() {
    this.dialogRef.close();
    this._router.navigate(['/anuncios']);
  }

  efetuarPagamento() {
    const a = document.createElement('a');
    switch (this.data.tempo_dias) {
      case 30:
        a.href = 'https://pag.ae/7XqwnGTcM';
        break;
      case 90:
        a.href = 'https://pag.ae/7Xqwo9rur';
        break;
      case 360:
        a.href = 'https://pag.ae/7XqwowLHK';
        break;
      default:
        break;
    }

    a.target = '_blank';
    a.click();
  }
}
