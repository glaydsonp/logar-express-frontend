import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnunciosRoutingModule } from './anuncios-routing.module';
import { AnunciosComponent } from './anuncios.component';
import { NovoAnuncioComponent } from './novo-anuncio/novo-anuncio.component';
import { NovoAnuncioDialogComponent } from './novo-anuncio-dialog/novo-anuncio-dialog.component';

@NgModule({
  declarations: [
    AnunciosComponent,
    NovoAnuncioComponent,
    NovoAnuncioDialogComponent
  ],
  imports: [CommonModule, AnunciosRoutingModule, SharedModule]
})
export class AnunciosModule {}
