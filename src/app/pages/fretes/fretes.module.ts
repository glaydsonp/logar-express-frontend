import { FretesDetalhesComponent } from './fretes-detalhes/fretes-detalhes.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FretesRoutingModule } from './fretes-routing.module';
import { FretesComponent } from './fretes.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [FretesComponent, FretesDetalhesComponent],
  imports: [CommonModule, FretesRoutingModule, SharedModule]
})
export class FretesModule {}
