import { ShippingService } from 'src/app/services/shipping.service';
import { FretesDetalhesComponent } from './fretes-detalhes/fretes-detalhes.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IShipping } from 'src/app/models/shipping';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fretes',
  templateUrl: './fretes.component.html',
  styleUrls: ['./fretes.component.scss']
})
export class FretesComponent implements OnInit {
  fretes: IShipping[] = [];

  constructor(
    public dialog: MatDialog,
    public shippingService: ShippingService
  ) {}

  ngOnInit(): void {
    this.getFretes();
  }

  openDialog(frete: IShipping) {
    const dialogRef = this.dialog.open(FretesDetalhesComponent, {
      data: { frete }
    });

    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog result: ${result}`);
    // });
  }

  getFretes() {
    this.shippingService.getAll().subscribe((res) => {
      this.fretes = res.fretes;
    });
  }

  getImageLink(frete: IShipping) {
    return `${environment.baseUrl}/imagem/${frete.logo}`;
  }
}
