import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IShipping } from 'src/app/models/shipping';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fretes-detalhes',
  templateUrl: './fretes-detalhes.component.html',
  styleUrls: ['./fretes-detalhes.component.scss']
})
export class FretesDetalhesComponent implements OnInit {
  frete: IShipping;

  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: { frete: IShipping }
  ) {
    this.frete = this.data.frete;
  }


  ngOnInit() {}

  close() {
    this.dialog.closeAll();
  }

  contato() {
    const a = document.createElement('a');
    a.href = `tel:+55${this.frete.celular_coleta}`;
    a.target = '_blank';
    a.click();
    a.remove();
  }

  whatsapp() {
    const a = document.createElement('a');
    a.href = `https://api.whatsapp.com/send?phone=55${this.frete.celular_coleta}`;
    a.target = '_blank';
    a.click();
    a.remove();
  }

  getImageLink() {
    return `${environment.baseUrl}/imagem/${this.frete.logo}`;
  }
}
