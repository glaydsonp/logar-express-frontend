import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { FormTypeEnum } from 'src/app/models/formType.enum';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { RegisterService } from 'src/app/services/register.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  form = new FormGroup({});

  formTypeEnum = FormTypeEnum;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private registerService: RegisterService,
    private snackBarService: SnackbarService
  ) {
    this.createForm();
  }

  ngOnInit(): void {}

  createForm() {
    this.form = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      formType: [null, Validators.required]
    });
  }

  sendForm() {
    const formType = +this.form.get('formType')?.value;

    this.registerService.setForm(this.form);

    if (formType && formType === this.formTypeEnum.Transportadora) {
      this.router.navigate(['/cadastro/transportadora']);
    } else if (formType && formType === this.formTypeEnum.Caminhoneiro) {
      this.router.navigate(['/cadastro/caminhoneiro']);
    } else if (formType && formType === this.formTypeEnum.Anunciante) {
      this.registerService
        .cadastrarAnunciante({
          nome: this.form.get('name')?.value,
          email: this.form.get('email')?.value,
          password: this.form.get('password')?.value
        })
        .subscribe(
          (res) => {
            this.snackBarService.openSnackBar(
              res.mensagem,
              SnackbarType.Success
            );
          },
          (err) => {
            this.snackBarService.openSnackBar(
              err.error.mensagem,
              SnackbarType.Danger
            );
          }
        );
    }
  }
}
