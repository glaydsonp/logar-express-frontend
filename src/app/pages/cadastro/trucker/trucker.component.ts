import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import { TruckerService } from 'src/app/services/trucker.service';
import { SnackbarType } from 'src/app/models/snackbar-type.enum';
import { FormTypeEnum } from 'src/app/models/formType.enum';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { MarcasService } from 'src/app/services/marcas.service';
import { EstadosService } from 'src/app/services/estados.service';

@Component({
  selector: 'app-trucker',
  templateUrl: './trucker.component.html',
  styleUrls: ['./trucker.component.scss']
})
export class TruckerComponent implements OnInit {
  form = new FormGroup({});
  marcas = [
    'AGRALE',
    'CITROEN',
    'DAF',
    'FIAT',
    'FORD',
    'FOTON',
    'HYUNDAI',
    'INTERNATIONAL',
    'IVECO',
    'KIA',
    'MAN',
    'MERCEDES',
    'PEUGEOT',
    'RENAULT',
    'SCANIA',
    'SINOTRUK1',
    'VOLKSWAGEN',
    'VOLVO'
  ];

  modelos: string[] = [];

  registerType = new FormControl(null);

  options = [
    {
      description: 'Sim',
      value: true
    },
    {
      description: 'Não',
      value: false
    }
  ];

  constructor(
    private _fb: FormBuilder,
    private _registerService: RegisterService,
    private _router: Router,
    public truckerService: TruckerService,
    private _snackbarService: SnackbarService,
    private marcasService: MarcasService,
    public estadosService: EstadosService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getPersonalInfo();
  }

  getPersonalInfo() {
    this._registerService
      .getForm()
      .subscribe((val) => {
        if (!val.value.name) {
          this._router.navigate(['/cadastro']);
          return;
        }

        this.form.get('personal')?.patchValue(val.value);
      })
      .unsubscribe();
  }

  createForm() {
    this.form = this._fb.group({
      personal: this._fb.group({
        name: ['Caminhoneiro Teste'],
        password: ['123456'],
        email: ['emailcaminhoneiro1@email.com']
      }),
      trucker: this._fb.group({
        cpf: ['61099753384', Validators.required],
        cnh: ['00000000000', Validators.required],
        data_emissao_cnh: ['10/10/2020', Validators.required],
        estado_expeditor_cnh: ['SP', Validators.required],
        telefone: ['0000000000', Validators.required],
        celular: ['00000000000', Validators.required],
        possui_mopp: [true, Validators.required]
      }),
      truck: this._fb.group({
        marca: ['Ford', Validators.required],
        modelo: ['Modelo Teste', Validators.required],
        cor: ['Vermelho', Validators.required],
        placa: ['ABC-1234', Validators.required],
        ano: ['2020', Validators.required],
        renavam: ['222222222', Validators.required],
        tipo_veiculo: ['Veículo Teste', Validators.required],
        tipo_carreta: ['Carreta Teste', Validators.required],
        tipo_carroceria: ['Carroceria Teste', Validators.required]
      })
    });

    this.form
      .get('truck')
      ?.get('marca')
      ?.valueChanges.subscribe((marcaSelecionada) => {
        this.modelos = this.marcasService.getModelos(marcaSelecionada);
      });
  }

  submit() {
    if (!this.form.valid) {
      this._snackbarService.openSnackBar(
        'Fomulário inválido!',
        SnackbarType.Danger
      );

      return;
    }

    const form = Object.assign(
      this.form.get('personal')?.value,
      this.form.get('trucker')?.value,
      this.form.get('truck')?.value
    );

    this._registerService.save(form, FormTypeEnum.Caminhoneiro).subscribe(
      (res) => {
        this._snackbarService.openSnackBar(res.mensagem, SnackbarType.Success);

        this._router.navigate(['/login']);
      },
      (error) => {
        const message = error.error.mensagem;

        this._snackbarService.openSnackBar(message, SnackbarType.Danger);
      }
    );
  }
}
