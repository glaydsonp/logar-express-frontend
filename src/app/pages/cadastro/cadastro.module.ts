import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadastroRoutingModule } from './cadastro-routing.module';
import { CadastroComponent } from './cadastro.component';
import { CompanyComponent } from './company/company.component';
import { TruckerComponent } from './trucker/trucker.component';

@NgModule({
  declarations: [CadastroComponent, CompanyComponent, TruckerComponent],
  imports: [CommonModule, CadastroRoutingModule, SharedModule]
})
export class CadastroModule {}
